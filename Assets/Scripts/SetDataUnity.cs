using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetDataUnity : MonoBehaviour
{

    string text;

    public void SetText(string newText)
    {
        text = newText;

        // Send this data back to Flutter that the app can read from.
        GetComponent<UnityMessageManager>().SendMessageToFlutter(text);

    }
    
}
